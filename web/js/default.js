$(function(){

/**
 * Side menu
 */

var sideMenuIsOpen = false;
var sideMenuYAnchor = 0;

function side_menu_open() {
  sideMenuYAnchor = window.scrollY + 120;
  $('#side_menu').css({paddingTop: sideMenuYAnchor})
  $('#side_menu').addClass('side_menu-visible');
  $(document).on('click', side_menu_close);
  sideMenuIsOpen = true;
}

function side_menu_close() {
  $(document).off('click', side_menu_close);
  $('#side_menu').removeClass('side_menu-visible');
  sideMenuIsOpen = false;
}

function side_menu_follow_scroll() {
  var newAnchor = window.scrollY + 120;
  if(sideMenuIsOpen && (newAnchor < sideMenuYAnchor)) {
    sideMenuYAnchor = newAnchor;
    $('#side_menu').css({paddingTop: newAnchor });
  }
}

function side_menu_reset() {
  $('#side_menu').css({paddingTop: sideMenuYAnchor});
}

$('[data-action=side_menu-open]').click(function(){ side_menu_open(); return false; });
$('[data-action=side_menu-close]').click(function(){ side_menu_close(); return false; });
$(window).on('scroll', side_menu_follow_scroll);

AOS.init({
  disable: 'phone'
});

});

var PSModal = (function(){

  /**
   * Opens a modal dialog
   * @param options object with the following keys
   *  - text: Modal dialog text (may include markup)
   *  - actionText: Text for the action button
   *  - actionCallback: Callback to call when pressing the action button
   */
  function open(options) {

    $modal = $("<div class='modal'></div>");

    $closeButton = $("<a href='#' class='close'></a>")
      .on('click', close);
    $modal.append($closeButton);

    $text = $("<div class='modal-text'></div>")
      .html(options.text);
    $modal.append($text);

    $button = $("<a href='#' class='button'></a>")
      .on('click', function() {
        close();
        options.actionCallback();
        return false;
      })
      .html(options.actionText);
    $modal.append($button);

    $('body').addClass('modal-visible');
    $('body').append($modal);
  }

  function close() {
    $('body').removeClass('modal-visible');
    $('.modal').remove();
    return false;
  }

  return {
    open: open,
    close: close
  };
})();