var PSModal = (function(){

  /**
   * Opens a modal dialog
   * @param options object with the following keys
   *  - text: Modal dialog text (may include markup)
   *  - actionText: Text for the action button
   *  - actionCallback: Callback to call when pressing the action button
   */
  function open(options) {

    $modal = $("<div class='modal'></div>");

    $closeButton = $("<a href='#' class='close'></a>")
      .on('click', close);
    $modal.append($closeButton);

    $text = $("<div class='modal-text'></div>")
      .html(options.text);
    $modal.append($text);

    $button = $("<a href='#' class='button'></a>")
      .on('click', function() {
        close();
        options.actionCallback();
        return false;
      })
      .html(options.actionText);
    $modal.append($button);

    $('body').addClass('modal-visible');
    $('body').append($modal);
  }

  function close() {
    $('body').removeClass('modal-visible');
    $('.modal').remove();
    return false;
  }

  return {
    open: open,
    close: close
  };
})();