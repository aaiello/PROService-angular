# Project Name

PRO Service - FrontEnd

## Installation

1) Usar dockerfile
	docker build -t proserviceimage .
	
## Usage

<br>docker run --name proserviceinstance -p 3000:80 -e "ENV=development" -d proserviceimage
<br>Los valores válidos para ENV son: development, integration, preproduction, production
<br>El sistema buscará las url de los web service en el archivo config.ENV.json siendo ENV el valor ingresado al ejecutar el docker run.
## Contributing

TODO: Write contributing

## History

TODO: Write history

## Credits

TODO: Write credits

## License

TODO: Write license