############################################################
# Dockerfile to build nginx (https://hub.docker.com/_/nginx/)
############################################################

# Set the base image to nginx
FROM nginx


# File Author / Maintainer
MAINTAINER Ismael Luque <ismael.luque@ogilvy.com>

################## BEGIN INSTALLATION ######################

# Copy all files to web server html and conf
COPY /web /usr/share/nginx/html
COPY /nginx.conf /etc/nginx/nginx.conf

##################### INSTALLATION END #####################

COPY docker-entrypoint.sh /
RUN ["chmod", "+x", "/docker-entrypoint.sh"]

ENTRYPOINT ["/docker-entrypoint.sh"]
CMD ["nginx", "-g", "daemon off;"]

RUN echo 'Finish'

