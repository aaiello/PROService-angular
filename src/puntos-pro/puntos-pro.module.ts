import { NgModule } from '@angular/core';
import { CommonModule }  from '@angular/common';
import { HttpModule } from '@angular/http';
import { FormsModule }   from '@angular/forms';
import { PuntosPROComponent } from './puntos-pro.component';
import { PuntosPRORoutingModule } from "./puntos-pro-routing.module";

@NgModule({
    imports: [
        CommonModule,
        HttpModule,
        FormsModule,
        PuntosPRORoutingModule,
    ],
    declarations: [
        PuntosPROComponent
    ],
    bootstrap: [ PuntosPROComponent ]
})
export class PuntosPROModule { }