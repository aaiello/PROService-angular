import { Ruta } from './ruta';

export class PuntoPRO {
    agencia : string;
    direccion : string;
    cp : string;
    provincia : string;
    contactoNombre : string;
    contactoCargo : string;
    telefono : string;
    movil : string;
    mail : string;
    urlFotoInterior : string;
    urlMapaAgencia : string;
    rutas : Ruta[];
}
