import { Component } from '@angular/core';
import { DomSanitizer, SafeResourceUrl, SafeUrl} from '@angular/platform-browser';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { PuntosPROService } from './puntos-pro.service';
import { PuntoPRO } from './punto-pro';

@Component({
    selector: 'ps-puntos-pro',
    templateUrl: './puntos-pro.component.html',
    styleUrls: [ ],
    providers: [ PuntosPROService ],
})
export class PuntosPROComponent {

    constructor(
        private sanitizer: DomSanitizer,
        private puntosPROService : PuntosPROService,
        private router : Router,
        private route: ActivatedRoute) {
    }

    searchString : string = '';
    loading : boolean = false;
    punto : PuntoPRO = null;
    puntoSearchString : string = '';
    selectedRuta = 0;

    ngOnInit() {
        const codigoPostalParam = this.route.params.subscribe((params:Params) => {
            if ( params['codigo-postal']) {
                this.search(params['codigo-postal']);
            }
        })
    }

    search(codigoPostal : string) {
        if (this.loading) {
            return false;
        }
        this.loading = true;
        this.searchString = codigoPostal;
        this.puntosPROService
            .getPunto(codigoPostal)
            .subscribe((punto) => {
                this.punto = punto;
                if (punto) {
                    this.puntoSearchString = codigoPostal;
                } else {
                    this.puntoSearchString = '';
                }
                this.selectedRuta = 0;
                this.loading = false;
            });
        return false;
    }

    clearSearch() {
        this.searchString = '';
        this.puntoSearchString = '';
        this.punto = null;
        this.selectedRuta = 0;
        return false;
    }

    selectRuta(index : number) {
        this.selectedRuta = index;
        return false;
    }

    getMapImage(index : number) {
        return this.safeImage(this.punto.rutas[index].urlMapa);
    }

    safeImage(url : string) {
        return this.sanitizer.bypassSecurityTrustStyle('url("' + url + '")');
    }

    submit() {
        this.router.navigate(['/puntos-proservice', this.searchString], {queryParams: {"top": false}});
    }
}