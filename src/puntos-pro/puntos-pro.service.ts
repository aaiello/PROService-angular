import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { PuntoPRO } from './punto-pro';
import { Ruta } from './ruta';
import {AppConfig} from "../ps-site/app.config";

declare var PSModal: any;

@Injectable()
export class PuntosPROService {

    constructor (private http: Http, private config : AppConfig) {}

    getPunto(cp : string) : Observable<PuntoPRO> {
        return this.http.get(`${this.config.get('puntosPRO.API')}/${cp}`)
            .map(this.mapPuntoPRO.bind(this))
            .catch(this.handleError);
    }

    private toPuntoPRO(data : any) {

        let puntoPRO = <PuntoPRO>({
            agencia : data.agencia,
            direccion : data.direccion,
            cp : ("00000" + data['cp-agencia']).substr(-5,5),
            provincia : data.provincia,
            contactoNombre : data['contacto-nombre'],
            contactoCargo : data['contacto-cargo'],
            telefono : data.telefono,
            movil : data.movil,
            mail : data.mail,
            urlFotoInterior : data['foto-interior'],
            urlMapaAgencia : data['mapa-agencia'],
            rutas: [],
        });

        for (let ruta of data.rutas) {
            puntoPRO.rutas.push(<Ruta>({
                nombre: ruta.nombre,
                nivelServicio: ruta['nivel-servicio'],
                urlMapa: ruta['mapa']
            }));
        }

        return puntoPRO;
    }

    private mapPuntoPRO(response:Response) : PuntoPRO {
        let data = response.json();
        if (data.errorNumber === 0 && data.errorCode === "ERROR_CP") {
            PSModal.open({
                text: `<p>El código postal ingresado no es válido.</p>`,
                actionText: 'Aceptar',
                actionCallback: () => {return false;}
            });
        }
        else if (data.errorNumber === 0 && data.errorCode === "ERROR_INEXISTENTE") {
            PSModal.open({
                text: `<p>Lo sentimos, actualmente no ofrecemos cobertura en tu zona geográfica.</p>
<p>Contacta con nosotros y te informaremos de las posibles soluciones.</p>`,
                actionText: 'Contacta',
                actionCallback: () => {window.location.href = '/contacto'; return false;}
            });
        }
        else if (data.errorNumber === 0 && data.errorCode === "ERROR_PROXIMAMENTE") {
            PSModal.open({
                text: `<p>Próximamente abriremos un Punto de Recambios Originales PRO Service cerca de tu código postal. Déjanos tus datos de contacto y te mantendremos informado.</p>

<p>Contacta con nosotros y te informaremos de la fecha exacta.</p>`,
                actionText: 'Contacta',
                actionCallback: () => {window.location.href = '/contacto'; return false;}
            });
        }
        else if (data.errorNumber !== 1) {
            PSModal.open({
                text: `<p>Error inesperado (${data.errorCode}) : ${data.errorMessage} </p>`,
                actionText: 'Contacta',
                actionCallback: () => {window.location.href = '/contacto'; return false;}
            });
        }

        if (data.errorNumber !== 1) {
            this.handleError(response);
        } else {
            return this.toPuntoPRO(data);
        }
    }

    private handleError (error: Response | any) {
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    }
}