import { NgModule }              from '@angular/core';
import { RouterModule, Routes}  from '@angular/router';
import { PuntosPROComponent } from './puntos-pro.component';

const appRoutes: Routes = [
    {
        path: '',
        component: PuntosPROComponent,
        data: { },
    },
    {
        path: ':codigo-postal',
        component: PuntosPROComponent,
        data: { },
    }
];
@NgModule({
    imports: [
        RouterModule.forChild(appRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class PuntosPRORoutingModule {}