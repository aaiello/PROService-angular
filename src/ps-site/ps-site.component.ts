import { Component } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import {Router, ActivatedRoute, NavigationEnd, Data} from '@angular/router';
import { MetaService } from '@nglibs/meta';
import 'rxjs/add/operator/switchMap';
declare var side_menu_reset:any;
declare var AOS:any;

@Component({
    selector: 'ps-site',
    templateUrl: './ps-site.component.html',
})
export class PSSiteComponent {

    page_class : string;
    showHeader : boolean;
    showFooter : boolean;

    constructor(private route : ActivatedRoute, private router : Router, private metadata: MetaService) { }

    ngOnInit() {
        // Scroll to top of page on route change
        this.router.events.subscribe((evt) => {
            if (!(evt instanceof NavigationEnd)) {
                return;
            }
            let params = new URLSearchParams(window.location.search.substring(1));
            if (params.get('top') !== 'false') {
                window.scrollTo(0,0);
            }
            this.page_class = 'page' + this.router.url.toString().replace('/', '-').split('?')[0];
            AOS.init({
                disable: 'phone'
            });

            this.showHeader = this.route.firstChild.snapshot.data['showHeader'] !== undefined ?
                this.route.firstChild.snapshot.data['showHeader'] : true;
            this.showFooter = this.route.firstChild.snapshot.data['showFooter'] !== undefined ?
                this.route.firstChild.snapshot.data['showFooter'] : true;
        });
    }

}