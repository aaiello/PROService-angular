import { Component, OnInit, ElementRef, AfterViewInit} from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd, Params, Data } from '@angular/router';
import 'rxjs/add/operator/switchMap';
declare var $:any;

@Component({
    selector: 'ps-header',
    templateUrl: 'header.component.html',
})
export class HeaderBlockComponent implements OnInit, AfterViewInit {

    pageTitle : string;
    $sideMenu : any;
    sideMenuYAnchor : number = 0;
    sideMenuIsOpen : boolean = false;


    constructor(private route: ActivatedRoute,
                private router: Router,
                private ref : ElementRef) {

        this.pageTitle = '';
        this.sideMenuClose = this.sideMenuClose.bind(this);
    }

    ngOnInit() {
        this.router.events.subscribe((evt) => {
            if (evt instanceof NavigationEnd) {
                if (evt.urlAfterRedirects === '/') {
                    this.pageTitle = '';
                } else {
                    this.pageTitle = this.route.snapshot.children[0].data['meta'].title;
                }
            }
        });
    }

    ngAfterViewInit() {
        this.$sideMenu = $(this.ref.nativeElement).find('#side_menu');
        $(window).on('scroll', () => {
            const newAnchor = window.scrollY + 120;
            if(this.sideMenuIsOpen && (newAnchor < this.sideMenuYAnchor)) {
                this.sideMenuSetPosition(newAnchor);
            }
        });

        this.router.events.subscribe((evt) => {
            if (evt instanceof NavigationEnd) {
                this.sideMenuSetPosition(0);
            }
        });
    }

    sideMenuOpen(event : MouseEvent) {
        this.sideMenuSetPosition(window.scrollY + 120);
        this.$sideMenu.addClass('side_menu-visible');
        $(document).on('click', this.sideMenuClose);
        this.sideMenuIsOpen = true;
        event.stopPropagation();
        event.preventDefault();
    }

    sideMenuClose(event : MouseEvent) {
        $(document).off('click', this.sideMenuClose);
        this.$sideMenu.removeClass('side_menu-visible');
        this.sideMenuIsOpen = false;
        event.stopPropagation();
        event.preventDefault();
    }

    sideMenuSetPosition(y: number) {
        this.sideMenuYAnchor = y;
        this.$sideMenu.css({paddingTop: this.sideMenuYAnchor});
    }
}