import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import {AppConfig} from "../app.config";

declare var PSModal: any;

@Injectable()
export class PSFormularioMeetingpointService {

    public headers : Headers;

    constructor (private http: Http, private config : AppConfig) {
        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/json');
    }

    send(data : any) : Observable<Response> {
        return this.http.post(this.config.get('formularioMeetingpoint.API'), JSON.stringify(data), {headers: this.headers})
            .map(this.mapResponse)
            .catch(this.handleError);
    }

    private mapResponse(response : Response) {
        return response.json();
    }

    private handleError (error: Response | any) {
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    }
}