import { Component } from '@angular/core';
import { PSFormularioMeetingpointService } from './ps-formulario-meetingpoint.service';
import { PSValidation } from '../services/ps-validation.service';
import {NgForm} from "@angular/forms";
declare let PSModal : any;

@Component({
    selector: 'sec-formulario-meetingpoint',
    templateUrl: 'formulario-meetingpoint-sec.component.html',
    providers: [ PSFormularioMeetingpointService, PSValidation ]
})
export class FormularioMeetingpointSecComponent {

    sending : boolean = false;

    nombre : string;
    apellido : string;
    dni: string;
    email: string;
    cargo: string;
    negocio : string;
    cif : string;
    direccionEmpresa : string;
    telefonoMovilContacto : string;
    aceptoPolitica : boolean;

    constructor(private registroService : PSFormularioMeetingpointService, private validator : PSValidation) { }

    submitForm(form : NgForm) {
        if (this.sending) {
            return;
        }

        if (!this.validateForm()) {
            return;
        }

        this.sending = true;
        this.registroService.send({
            nombre: this.nombre,
            apellido: this.apellido,
            dni: this.dni,
            email: this.email,
            cargo: this.cargo,
            negocio: this.negocio,
            cif: this.cif,
            direccionEmpresa: this.direccionEmpresa,
            telefonoMovilContacto: this.telefonoMovilContacto,
        }).subscribe((response : any) => {
            this.sending = false;
            if (response.errorNumber !== 1) {
                PSModal.open({
                    text: `<p>Error enviando su registro (${response.errorCode}) : ${response.errorMessage} </p>`,
                    actionText: 'Aceptar',
                    actionCallback: () => {return false;}
                });
            } else {
                PSModal.open({
                    text: `<p>Sus datos han sido registrados exitosamente</p>`,
                    actionText: 'Aceptar',
                    actionCallback: () => {return false;}
                });
                form.reset();
            }
        });
    }

    private validateForm() {
        const requiredFields = {
            nombre: 'nombre',
            apellido: 'apellido',
            email: 'e-mail de contacto'
        };

        const missingFields : Array<any> = [];
        for (const requiredField in requiredFields) {
            if (! this[requiredField]) {
                missingFields.push(requiredFields[requiredField]);
            }
        }

        if (!this.telefonoMovilContacto) {
            missingFields.push('su teléfono fijo o móvil');
        }

        if (missingFields.length) {
            let errorMessage = "Debe ingresar " + missingFields.join(', ');
            let pos = errorMessage.lastIndexOf(',');
            errorMessage = errorMessage.substring(0,pos) + errorMessage.substring(pos).replace(',', ' y');
            errorMessage += '.';
            this.formError(errorMessage);
            return false;
        }

        if (!this.validator.validateEmail(this.email)) {
            this.formError("El e-mail ingresado no es válido.");
            return false;
        }

        if (this.telefonoMovilContacto && !this.validator.validateTelMovil(this.telefonoMovilContacto)) {
            this.formError("El teléfono móvil ingresado no es válido");
            return false;
        }

        if (!this.aceptoPolitica) {
            this.formError("Debe aceptar la política de privacidad para enviar la información.")
            return false;
        }

        return true;
    }

    private formError(errorMessage : string) {
        PSModal.open({
            text: `<p>${errorMessage}</p>`,
            actionText: 'Aceptar',
            actionCallback: () => {return false;}
        });
    }
}