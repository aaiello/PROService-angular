import { Component } from '@angular/core';

@Component({
    selector: 'page-formulario-meetingpoint',
    template: `
    <ps-header-solologo></ps-header-solologo>
    <sec-formulario-meetingpoint></sec-formulario-meetingpoint>
    <ps-footer-disclamer-meetingpoint></ps-footer-disclamer-meetingpoint>
  `,
})
export class FormularioMeetingpointPageComponent { }