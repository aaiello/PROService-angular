import { NgModule }              from '@angular/core';
import { RouterModule, Routes }  from '@angular/router';
import { MetaModule, MetaLoader, MetaStaticLoader, PageTitlePositioning } from '@nglibs/meta';
import { InicioPageComponent } from './pages/inicio-page.component';
import { AcercaPageComponent } from './pages/acerca-page.component';
import { PageNotFoundComponent } from './pages/not-found.component';
import { ContactaPageComponent } from "./pages/contacta-page.component";
import { GrandesCuentasPageComponent } from "./pages/grandes_cuentas-page.component";
import { ProductosPageComponent } from "./pages/productos-page.component";
import { UnetePageComponent } from "./pages/unete-page.component";
import { VentajasPageComponent } from "./pages/ventajas-page.component";
import { VinculacionPageComponent } from "./pages/vinculacion-page.component";
import { RegistroPageComponent } from "./pages/registro-page.component";
import { SuscripcionNewsletterPageComponent } from "./pages/suscripcion-newsletter-page.component";
import { BajaNewsletterPageComponent } from "./pages/baja-newsletter-page.component"
import { SoyMecanicoPageComponent } from "./pages/soymecanico-page.component";
import { NoticiasListaPageComponent } from "./pages/noticias-lista-page.component";
import { NoticiasDetallePageComponent } from "./pages/noticias-detalle-page.component";
import { NoticiasResolver } from "./services/ps-noticia-resolver.service";
import { OpenProGironaPageComponent } from"./pages/open-pro-girona-page.component";
import { OpenProBarcelonaoestPageComponent } from"./pages/open-pro-barcelonaoest-page.component";
import { OpenProTarragonaPageComponent } from"./pages/open-pro-tarragona-page.component";
import { FormularioMeetingpointPageComponent } from "./formularios/formulario-meetingpoint-page.component";

const appRoutes: Routes = [
    {
        path: '',
        component: InicioPageComponent,
        data: {
            meta: {
                title:'Punto de Recambios Originales',
                description:'PRO Service es el punto de recambios originales de Volkswagen Group, un servicio que se adapta a tus necesidades. ¡De profesional a profesional!'
            }
        },
    },
    {
        path: 'proservice',
        component: AcercaPageComponent,
        data: {
            meta: {
                title:'Acerca de nosotros',
                description:'PRO Service Volkswagen es tu punto de recambios originales. Un servicio profesional que se adapta a tus necesidades. ¡Con el compromiso de Volkswagen Group!'
            }
        },
    },
    {
        path: 'contacto',
        component: ContactaPageComponent,
        data: {
            meta: {
                title:'Contacto',
                description:'Contacta con tu punto PRO Service más cercano por teléfono o e-mail. Nuestro equipo de profesionales resolverá todas tus consultas rápidamente. ¡Dígame!'
            }
        },
    },
    {
        path: 'grandes-cuentas',
        component: GrandesCuentasPageComponent,
        data: {
            meta: {
                title:'Grandes cuentas',
                description:'Buscamos sinergias con las grandes cuentas de la automoción para mejorar juntos. Trabaja con PRO Service, de Volkswagen Group, y añade valor a tu negocio. ¡Infórmate!'
            }
        },
    },
    {
        path: 'productos-y-servicios',
        component: ProductosPageComponent,
        data: {
            meta: {
                title:'Productos y servicios',
                description:'Trabaja con la gama de productos y servicios PRO Service. Recambios de gran calidad a precios competitivos con la garantía y experiencia de Volkswagen Group. '
            }
        },
    },
    {
        path: 'alta',
        component: UnetePageComponent,
        data: {
            meta: {
                title:'Proceso de alta',
                description:'Únete a PRO Service, de Volkswagen Group, en tres sencillos pasos y aprovecha todas las ventajas de trabajar junto a un grupo líder. ¡De profesional a profesional!'
            }
        },
    },
    {
        path: 'ventajas-taller-volkswagen',
        component: VentajasPageComponent,
        data: {
            meta: {
                title:'Ventajas taller PRO Service',
                description:'Descubre las ventajas de Volkswagen PRO Service y promueve la excelencia en el servicio y la gestión de tu taller. ¡De profesional a profesional!'
            }
        },
    },
    {
        path: 'programa-vinculacion',
        component: VinculacionPageComponent,
        data: {
            meta: {
                title:'Programa de vinculación',
                description:'Descubre el programa de vinculación PRO Service y benefíciate de las ventajas exclusivas de formar parte de Volkswagen Group. ¡De profesional a profesional!'
            }
        },
    } ,
    {
        path: 'puntos-proservice',
        loadChildren: '../puntos-pro/puntos-pro.module#PuntosPROModule',
        data: {
            meta: {
                title: 'Puntos PRO Service',
                description: 'Encuentra tus puntos PRO Service más cercanos y pon a punto tu vehículo con rapidez. Un servicio profesional con el compromiso de Volkswagen group.'
            },
        },
    },
    {
        path: 'suscripcion-newsletter',
        component: SuscripcionNewsletterPageComponent,
        data: {
            showHeader: true,
            showFooter: true,
            meta: {
                title: 'Apúntante a nuestra Newsletter',
            }
        }
    },
    {
        path: 'baja-newsletter',
        component: BajaNewsletterPageComponent,
        data: {
            showHeader: false,
            showFooter: false,
            meta: {
                title: 'Baja de newsletter',
            }
        }
    },
    {
        path: 'formulario-visitantes',
        component: RegistroPageComponent,
        data: {
            showHeader: false,
            showFooter: false,
            meta: {
                title: 'Formulario de registro',
            }
        }
    },
    {
        path: 'formulario-soy-mecanico',
        component: SoyMecanicoPageComponent,
        data: {
            showHeader: false,
            showFooter: false,
            meta: {
                title: 'Formulario de registro #soymecanico',
            }
        }
    },
    {
        path: 'open-pro-tarragona',
        component: OpenProTarragonaPageComponent,
        data: {
            showHeader: false,
            showFooter: false,
            meta: {
                title: 'Formulario de registro apertura PRO Service Tarragona',
            }
        }
    },
    {
        path: 'open-pro-girona',
        component: OpenProGironaPageComponent,
        data: {
        showHeader: false,
            showFooter: false,
            meta: {
                title: 'Formulario de registro apertura PRO Service Girona',
            }
        }
    },
    {
        path: 'open-pro-barcelona-oest',
            component: OpenProBarcelonaoestPageComponent,
        data: {
        showHeader: false,
            showFooter: false,
            meta: {
                title: 'Formulario de registro apertura PRO Service Barcelona Oest',
            }
        }
    },
    {
        path: 'noticias-volkswagen-proservice',
        component: NoticiasListaPageComponent,
        resolve: {
            noticiaList: NoticiasResolver
        },
        data: {
            meta: {
                title: 'Noticias',
                description: 'Conoce las últimas noticias sobre PRO Service: aperturas, pruebas, recambios y todo lo que necesitas saber sobre Volkswagen Group',
            }
        }
    },
    {
        path: 'noticias-volkswagen-proservice/:noticia_id',
        component: NoticiasDetallePageComponent,
        resolve: {
            noticia: NoticiasResolver
        },
        data: {
            meta: {
                title: 'Noticias',
                description: 'Conoce las últimas noticias sobre PRO Service: aperturas, pruebas, recambios y todo lo que necesitas saber sobre Volkswagen Group',
            }
        }
    },
    {
        path: 'noticias-volkswagen-proservice/etiqueta/:tag_id',
        component: NoticiasListaPageComponent,
        resolve: {
            noticiaList: NoticiasResolver
        },
        data: {
            meta: {
                title: 'Noticias',
                description: 'Conoce las últimas noticias sobre PRO Service: aperturas, pruebas, recambios y todo lo que necesitas saber sobre Volkswagen Group',
            }
        }
    },
    {
        path: 'pro-service-meeting-point',
        component: FormularioMeetingpointPageComponent,
        data: {
            showHeader: false,
            showFooter: false,
            meta: {
                title: 'Formulario de registro',
            }
        }
    },
    {
        path: '**',
        component: PageNotFoundComponent,
        data: { title: 'Página no encontrada' },
    }
];

export function metadataFactory() {
    return new MetaStaticLoader({
        pageTitlePositioning: PageTitlePositioning.PrependPageTitle,
        pageTitleSeparator: ' - ',
        applicationName: 'PRO Service Volkswagen Group',
        defaults: {
            title: 'PRO Service Volkswagen Group',
            description: 'PRO Service es el punto de recambios originales de Volkswagen Group, un servicio que se adapta a tus necesidades. ¡De profesional a profesional!',
            'og:type': 'website',
            'og:locale': 'es_ES'
        }
    });
}

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes),
        MetaModule.forRoot({
            provide: MetaLoader,
            useFactory: (metadataFactory)
        })
    ],
    exports: [
        RouterModule
    ],
    providers: [
        NoticiasResolver,
    ]
})
export class PSRoutingModule {}