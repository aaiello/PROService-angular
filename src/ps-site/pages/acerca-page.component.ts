import { Component } from '@angular/core';

@Component({
  selector: 'page-acerca',
  template: `
    <sec-acerca-tu_punto></sec-acerca-tu_punto>
    <sec-acerca-compromiso></sec-acerca-compromiso>
    <sec-acerca-pro_a_pro></sec-acerca-pro_a_pro>
    <sec-inicio-ultimas_noticias></sec-inicio-ultimas_noticias>
    <sec-inicio-mas_info></sec-inicio-mas_info>
  `,
})
export class AcercaPageComponent { }