import { Component } from '@angular/core';

@Component({
    selector: 'page-vinculacion',
    template: `
    <sec-vinculacion></sec-vinculacion>
  `,
})
export class VinculacionPageComponent { }