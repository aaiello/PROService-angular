import { Component } from '@angular/core';

@Component({
    selector: 'page-noticias-lista',
    template:
        `<sec-noticias-lista></sec-noticias-lista>`,
})
export class NoticiasListaPageComponent { }