import { Component } from '@angular/core';

@Component({
    selector: 'page-unete',
    template: `
    <sec-unete-pasos></sec-unete-pasos>
    <sec-unete-premio></sec-unete-premio>
    <sec-inicio-mas_info></sec-inicio-mas_info>
  `,
})
export class UnetePageComponent { }