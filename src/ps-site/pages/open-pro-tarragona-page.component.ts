import { Component } from '@angular/core';

@Component({
    selector: 'open-pro-tarragona',
    template: `
    <ps-header-solologo></ps-header-solologo>
    <sec-open-pro-tarragona-form></sec-open-pro-tarragona-form>
    <ps-footer-disclamer></ps-footer-disclamer>
  `,
})
export class OpenProTarragonaPageComponent { }