import { Component } from '@angular/core';

@Component({
    selector: 'open-pro-girona',
    template: `
    <ps-header-solologo></ps-header-solologo>
    <sec-open-pro-girona-form></sec-open-pro-girona-form>
    <ps-footer-disclamer></ps-footer-disclamer>
  `,
})
export class OpenProGironaPageComponent { }