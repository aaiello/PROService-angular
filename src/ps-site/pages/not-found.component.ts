import { Component } from '@angular/core';

@Component({
    selector: 'page-not-found',
    template: `
    <sec-not-found></sec-not-found>
  `,
})
export class PageNotFoundComponent { }