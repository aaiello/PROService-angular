import { Component } from '@angular/core';

@Component({
    selector: 'page-suscripcion-newsletter',
    template: `        
    <sec-suscripcion-newsletter-form></sec-suscripcion-newsletter-form>
  `,
})
export class SuscripcionNewsletterPageComponent { }