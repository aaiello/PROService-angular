import { Component } from '@angular/core';

@Component({
    selector: 'page-productos',
    template: `
    <sec-productos-servicio></sec-productos-servicio>
    <sec-productos-lista></sec-productos-lista>
  `,
})
export class ProductosPageComponent { }