import { Component } from '@angular/core';

@Component({
    selector: 'page-registro',
    template: `
    <ps-header-solologo></ps-header-solologo>
    <sec-soymecanico-form></sec-soymecanico-form>
    <ps-footer-disclamer></ps-footer-disclamer>
  `,
})
export class SoyMecanicoPageComponent { }