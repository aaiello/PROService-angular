import { Component } from '@angular/core';

@Component({
    selector: 'page-registro',
    template: `
    <ps-header-solologo></ps-header-solologo>
    <sec-registro-form></sec-registro-form>
    <ps-footer-disclamer></ps-footer-disclamer>
  `,
})
export class RegistroPageComponent { }