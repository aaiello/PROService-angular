import { Component } from '@angular/core';

@Component({
    selector: 'open-pro-barcelonaoest',
    template: `
    <ps-header-solologo></ps-header-solologo>
    <sec-open-pro-barcelonaoest-form></sec-open-pro-barcelonaoest-form>
    <ps-footer-disclamer></ps-footer-disclamer>
  `,
})
export class OpenProBarcelonaoestPageComponent { }