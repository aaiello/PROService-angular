import { Component } from '@angular/core';

@Component({
    selector: 'page-inicio',
    template: `
    <sec-inicio-slider></sec-inicio-slider>
    <!--<sec-inicio-pro_a_pro></sec-inicio-pro_a_pro>-->
    <sec-inicio-trabajar></sec-inicio-trabajar>
    <!--<sec-inicio-accordion></sec-inicio-accordion>-->
    <sec-inicio-productos></sec-inicio-productos>
    <sec-inicio-youtube></sec-inicio-youtube>
    <sec-inicio-ventajas></sec-inicio-ventajas>
    <!--<sec-inicio-seesaw></sec-inicio-seesaw>-->
    <sec-inicio-ultimas_noticias></sec-inicio-ultimas_noticias>
    <sec-inicio-unete></sec-inicio-unete>
    <sec-inicio-mas_info></sec-inicio-mas_info>
`,
})
export class InicioPageComponent { }
