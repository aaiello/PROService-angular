import { Component } from '@angular/core';

@Component({
    selector: 'page-contacta',
    template: `
    <!--<sec-contacta-telefono></sec-contacta-telefono>-->
    <sec-contacta-email></sec-contacta-email>
  `,
})
export class ContactaPageComponent { }