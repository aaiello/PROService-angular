import { Component } from '@angular/core';

@Component({
    selector: 'page-baja-newsletter',
    template: `        
    <sec-baja-newsletter-form></sec-baja-newsletter-form>
  `,
})
export class BajaNewsletterPageComponent { }