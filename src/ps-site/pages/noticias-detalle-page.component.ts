import { Component } from '@angular/core';

@Component({
    selector: 'page-noticias-detalle',
    template: `<sec-noticias-detalle></sec-noticias-detalle>
    <sec-inicio-ultimas_noticias></sec-inicio-ultimas_noticias>`,
})
export class NoticiasDetallePageComponent { }