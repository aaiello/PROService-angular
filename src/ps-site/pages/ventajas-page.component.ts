import { Component } from '@angular/core';

@Component({
    selector: 'page-ventajas',
    template: `
    <sec-ventajas-por_que></sec-ventajas-por_que>
    <sec-ventajas-lista></sec-ventajas-lista>
  `,
})
export class VentajasPageComponent { }