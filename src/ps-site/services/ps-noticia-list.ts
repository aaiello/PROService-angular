import { Noticia } from './ps-noticia';

export class NoticiaList {
    searchTerm : string;
    searchTermSlug : string;

    noticias : Array<Noticia> = [];
}