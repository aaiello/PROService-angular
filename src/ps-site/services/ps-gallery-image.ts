import {SafeStyle, SafeUrl} from "@angular/platform-browser";

export class GalleryImage {
    url : string;
    href : string;
    responsive : string;
    safe_url : SafeStyle;
    safe_href : SafeUrl;
}
