import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { GalleryImage } from './ps-gallery-image';
import {AppConfig} from "../app.config";

declare var PSModal: any;

@Injectable()
export class GalleryImageService {

    constructor (private http: Http, private config : AppConfig) {}

    getGalleryImages() : Observable< Array<GalleryImage> > {
        return this.http.get(this.config.get('gallery.API'))
            .map(this.mapGalleryImages.bind(this));
    }

    private toGalleryImage(data : any) : GalleryImage {

        let galleryImage : GalleryImage = new GalleryImage();
        galleryImage.url = data.url;
        galleryImage.href = data.link;
        galleryImage.responsive = data.responsive;

        return galleryImage;
    }

    private mapGalleryImages(response:Response) : Array<GalleryImage> {
        let data = response.json();

        if (data.length) {
            const imageList = [];
            for (const item of data) {
                imageList.push(this.toGalleryImage(item));
            }
            return imageList;
        } else {
            return this.handleError(response);
        }
    }

    private handleError (error: Response | any) : Array<GalleryImage>  {
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return this.defaultImages();
    }

    private defaultImages() : Array<GalleryImage>  {
        const defaultDesktopImage = new GalleryImage();
        defaultDesktopImage.url = this.config.get('gallery.defaultDesktopImagePath');
        defaultDesktopImage.href = '#';
        defaultDesktopImage.responsive = 'desktop';

        const defaultMobileImage = new GalleryImage();
        defaultMobileImage.url = this.config.get('gallery.defaultMobileImagePath');
        defaultMobileImage.href = '#';
        defaultMobileImage.responsive = 'mobile';

        return [
            defaultDesktopImage,
            defaultMobileImage
        ];
    }
}