import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { AppConfig } from "../app.config";
import { Noticia } from "./ps-noticia";
import { TagNoticia } from "./ps-tag-noticia";
import { NoticiaList } from "./ps-noticia-list";

declare var PSModal: any;

@Injectable()
export class NoticiaService {

    constructor (private http: Http, private config : AppConfig, private sanitizer: DomSanitizer) {}

    getLatestNoticias() : Observable< NoticiaList > {

        return this.http.get(`${this.config.get('noticias.API')}/latest`)
            .map(this.mapNoticiaList.bind(this));
    }

    getAllNoticias() : Observable< NoticiaList > {

        return this.http.get(`${this.config.get('noticias.API')}/list`)
            .map(this.mapNoticiaList.bind(this));
    }

    getNoticia(slug : string) : Observable< Noticia > {

        return this.http.get(`${this.config.get('noticias.API')}/get/${slug}`)
            .map(this.mapNoticia.bind(this));
    }

    getNoticiasByTag(tag : string) : Observable< NoticiaList > {

        return this.http.get(`${this.config.get('etiquetasNoticias.API')}/get/${tag}`)
            .map(this.mapNoticiaList.bind(this));
    }

    private toNoticiaList(data : any) : NoticiaList {

        let noticiaList : NoticiaList = new NoticiaList();
        for (const noticiaData of data.noticias) {
            noticiaList.noticias.push(this.toNoticia(noticiaData));
        }

        if (data.categoryTitulo) {
            noticiaList.searchTerm = data.categoryTitulo;
        }
        if (data.categorySlug) {
            noticiaList.searchTermSlug = data.categorySlug;
        }

        return noticiaList;
    }

    private toNoticia(data : any) : Noticia {

        let noticia : Noticia = new Noticia();

        noticia.slug = data.slug;
        noticia.url = data.url;
        noticia.titulo = data.titulo;
        noticia.fechaPublicacion = new Date(data.publishedAt);
        noticia.fechaActualizacion = new Date(data.updatedAt);
        noticia.fotoThumb = data.fotoThumb;
        noticia.foto = data.foto;
        noticia.teaserText = data.bajada;
        noticia.cuerpo = data.cuerpo;
        noticia.fuente = data.fuente;

        noticia.safe_cuerpo = this.sanitizer.bypassSecurityTrustHtml(noticia.cuerpo);
        noticia.safe_teaserText = this.sanitizer.bypassSecurityTrustHtml(noticia.teaserText);

        noticia.safe_url = this.sanitizer.bypassSecurityTrustUrl(noticia.url);
        noticia.safe_foto = this.sanitizer.bypassSecurityTrustStyle('url("' + noticia.foto + '")');
        noticia.safe_fotoThumb = this.sanitizer.bypassSecurityTrustStyle('url("' + noticia.fotoThumb + '")');

        noticia.tags = [];
        if (data.etiquetas) {
            for (const item of data.etiquetas) {
                noticia.tags.push(this.toTagNoticia(item));
            }
        }

        noticia.relacionadas = [];
        if (data.relacionadas) {
            for (const item of data.relacionadas) {
                noticia.relacionadas.push(this.toNoticia(item));
            }
        }

        return noticia;
    }

    private toTagNoticia(data : any) : TagNoticia {
        let tag : TagNoticia = new TagNoticia();

        tag.titulo = data.titulo;
        tag.slug = data.slug;

        return tag;
    }

    private mapNoticia(response:Response) : Noticia {
        let data = response.json();

        // todo: Remove this fake related news thing
        let answer = this.toNoticia(data.noticias[0]);
        this.getLatestNoticias().subscribe((noticiaList : NoticiaList) => {
            answer.relacionadas = noticiaList.noticias;
        });

        return answer;
    }

    private mapNoticiaList(response : Response) : NoticiaList {
        let data = response.json();

        return this.toNoticiaList(data);
    }

    private handleError (error: Response | any) : Array<Noticia> {
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);

        return [];
    }
}