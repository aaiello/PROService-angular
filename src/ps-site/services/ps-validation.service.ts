import { Injectable } from '@angular/core';

@Injectable()
export class PSValidation {

    validateEmail(email : string) {
        let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

    validateTelMovil(tel : string) {
        let re =/^\s*[67](\s*\d){8}$/;
        return re.test(tel);
    }

    validateTelFijo(tel : string) {
        let re =/^\s*[89](\s*\d){8,12}$/;
        return re.test(tel);
    }

    validateCodigoPostal(cp : string) {
        let re = /^\d{5}$/;
        return re.test(cp);
    }
}