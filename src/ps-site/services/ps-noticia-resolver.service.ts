import { Injectable } from '@angular/core';
import { Router, Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Noticia } from './ps-noticia';
import { NoticiaService } from './ps-noticia.service';

@Injectable()
export class NoticiasResolver implements Resolve<any> {

    constructor(
        private noticiaService : NoticiaService ,
        private router: Router
    ) {}

    resolve(route:ActivatedRouteSnapshot) : Observable<any> | Promise<any> | any {

        if (route.params['noticia_id']) {
            return this.noticiaService.getNoticia(route.params['noticia_id']);
        } else if (route.params['tag_id']) {
            return this.noticiaService.getNoticiasByTag(route.params['tag_id']);
        } else {
            return this.noticiaService.getAllNoticias();
        }
    }
}