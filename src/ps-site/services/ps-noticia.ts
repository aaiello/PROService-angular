import {SafeHtml, SafeStyle, SafeUrl} from "@angular/platform-browser";
import {TagNoticia} from "./ps-tag-noticia";

export class Noticia {
    // Teaser and Full
    slug : string;
    url : string;
    titulo : string;
    fechaPublicacion : Date;
    fechaActualizacion : Date;
    fotoThumb : string;
    // Full
    foto : string;
    teaserText :string;
    cuerpo : string;
    fuente : string;
    tags : Array<TagNoticia>;
    relacionadas : Array<Noticia>;

    safe_cuerpo : SafeHtml;
    safe_teaserText : SafeHtml;
    safe_url : SafeStyle;
    safe_foto : SafeStyle;
    safe_fotoThumb : SafeStyle;
}
