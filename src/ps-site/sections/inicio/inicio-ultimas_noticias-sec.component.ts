import { Component } from '@angular/core';
import { NoticiaService } from "../../services/ps-noticia.service";
import { Noticia } from '../../services/ps-noticia';

@Component({
    selector: 'sec-inicio-ultimas_noticias',
    templateUrl: 'inicio-ultimas_noticias-sec.component.html',
    providers: [ NoticiaService ]
})
export class InicioUltimasNoticiasSecComponent {

    noticias : Noticia[];

    constructor(
        private noticiaService : NoticiaService) {
    }

    ngOnInit() {
        this.noticiaService.getLatestNoticias().subscribe((noticiaList) => {
            this.noticias = noticiaList.noticias;
        });
    }
}