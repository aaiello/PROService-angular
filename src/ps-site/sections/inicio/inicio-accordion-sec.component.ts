import {Component, ElementRef, AfterViewInit} from '@angular/core';
declare var $:any;

@Component({
    selector: 'sec-inicio-accordion',
    templateUrl: 'inicio-accordion-sec.component.html',
})
export class InicioAccordionSecComponent implements AfterViewInit {

    constructor(private ref:ElementRef) {
    }

    ngAfterViewInit() {

        var $accordion : any = $(this.ref.nativeElement);
        var $allItems : any = $accordion.find('.accordion-item');
        var shutTimeout : any = null;

        $allItems.hover(function() {
            clearTimeout(shutTimeout);
            shutTimeout = null;
            $(this)
                .removeClass('selected-before')
                .removeClass('selected-̄after')
                .addClass('selected');
            $(this).prevAll()
                .removeClass('selected-after')
                .removeClass('selected')
                .addClass('selected-before');
            $(this).nextAll()
                .removeClass('selected-before')
                .removeClass('selected')
                .addClass('selected-after');
        }, () => {
            clearTimeout(shutTimeout);
            shutTimeout = window.setTimeout(() => {
                $allItems
                    .removeClass('selected-before')
                    .removeClass('selected-after')
                    .removeClass('selected');
                shutTimeout = null;
            }, 500);
        });
    }
}