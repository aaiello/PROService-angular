import { Component, ElementRef, AfterViewInit } from '@angular/core';
declare var $:any;

@Component({
    selector: 'sec-inicio-seesaw',
    templateUrl: 'inicio-seesaw-sec.component.html',
})
export class InicioSeesawSecComponent implements AfterViewInit {

    constructor(private ref: ElementRef) {

    }

    ngAfterViewInit() {
        var $seesaw = $(this.ref.nativeElement);

        $seesaw.find('.seesaw-left').hover(function(){
            $(this).parent('.seesaw').removeClass('seesaw-over-right');
            $(this).parent('.seesaw').addClass('seesaw-over-left');
        }, function() {
            $(this).parent('.seesaw').removeClass('seesaw-over-left');
        });
        $seesaw.find('.seesaw-right').hover(function(){
            $(this).parent('.seesaw').removeClass('seesaw-over-left');
            $(this).parent('.seesaw').addClass('seesaw-over-right');
        }, function() {
            $(this).parent('.seesaw').removeClass('seesaw-over-right');
        });
    }
}