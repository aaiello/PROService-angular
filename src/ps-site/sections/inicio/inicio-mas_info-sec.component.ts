import { Component } from '@angular/core';
import {Router} from '@angular/router';

@Component({
    selector: 'sec-inicio-mas_info',
    templateUrl: 'inicio-mas_info-sec.component.html',
})
export class InicioMasInfoSecComponent {

    codigoPostal : string;

    constructor(private router : Router) {

    }

    findClosePunto() {
        this.router.navigate(['/puntos-proservice', this.codigoPostal]);
        return false;
    }
}