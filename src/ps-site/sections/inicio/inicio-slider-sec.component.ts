import { Component, AfterViewInit, HostListener } from '@angular/core';
import { DomSanitizer, SafeResourceUrl, SafeUrl} from '@angular/platform-browser';
import { GalleryImageService } from '../../services/ps-gallery-image.service';
import { GalleryImage } from "../../services/ps-gallery-image";
declare var $:any;

@Component({
    selector: 'sec-inicio-slider',
    templateUrl: 'inicio-slider-sec.component.html',
    providers: [ GalleryImageService ]
})
export class InicioSliderSecComponent {

    images : GalleryImage[];
    activeImages : GalleryImage[];
    responsiveMode : string;
    cycleInited : boolean = true;

    constructor(
        private sanitizer: DomSanitizer,
        private galleryImageService : GalleryImageService
    ) {
    }

    ngOnInit() {
        this.galleryImageService.getGalleryImages().subscribe((images) => {
            this.images = images;
            this.activateImages(this.getResponsiveMode());
        });
    }

    ngAfterViewChecked() {
        if (!this.cycleInited) {
            this.adjustSliderHeight();
            $('.cycle-slideshow').cycle();
            this.cycleInited = true;
        }
    }

    activateImages(responsiveMode : string) {
        this.activeImages = [];
        for (const image of this.images) {
            if (image.responsive === responsiveMode) {
                image.safe_url = this.sanitizer.bypassSecurityTrustStyle('url("' + image.url + '")');
                image.safe_href = this.sanitizer.bypassSecurityTrustUrl(image.href);
                this.activeImages.push(image);
            }
        }
        this.cycleInited = false;
    }

    getResponsiveMode() {
        if (window.innerWidth <= 768) {
            return 'mobile';

        } else {
            return 'desktop';
        }
    }

    adjustSliderHeight() {
        if (this.getResponsiveMode() == 'mobile') {
            const responsiveImageRatio = 611/414;
            const sliderHeight = window.innerWidth * responsiveImageRatio;
            $('.cycle-slideshow').css({height: sliderHeight});
            $('.section-slider').css({height: sliderHeight});
        }
    }

    @HostListener('window:resize', ['$event'])
    onResize(event : Event) {
        this.adjustSliderHeight();
        // TODO: Agregar handler resize. Problema: cycle2 le roba control de los elementos del slider a angular.
        // const currentMode = this.getResponsiveMode();
        // if (this.responsiveMode !== currentMode) {
        //     this.responsiveMode = currentMode;
        //     this.activateImages(currentMode);
        // }
    }
}