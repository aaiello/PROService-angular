import { Component } from '@angular/core';
import { PSContactService } from '../../services/ps-contact.service';
import { PSValidation } from '../../services/ps-validation.service';
import {NgForm} from "@angular/forms";
declare let PSModal : any;

@Component({
    selector: 'sec-contacta-email',
    templateUrl: 'contacta-email-sec.component.html',
    providers: [ PSContactService, PSValidation ]
})
export class ContactaEmailSecComponent {

    sending : boolean = false;

    nombre : string;
    apellido : string;
    email : string;
    sector : string = '';
    negocio : string;
    telefonoContacto : string;
    telefonoMovilContacto : string;
    codigoPostal : string;
    consulta : string;
    newsletter : boolean = true;

    constructor(private contactService : PSContactService, private validator : PSValidation) { }

    submitForm(form : NgForm) {
        if (this.sending) {
            return;
        }

        if (!this.validateForm()) {
            return;
        }

        this.sending = true;
        this.contactService.send({
            nombre: this.nombre,
            apellido: this.apellido,
            email: this.email,
            sector: this.sector,
            negocio: this.negocio,
            telefonoContacto: this.telefonoContacto,
            telefonoMovilContacto: this.telefonoMovilContacto,
            codigoPostal: this.codigoPostal,
            consulta: this.consulta,
            newsletter: this.newsletter,
        }).subscribe((response : any) => {
            this.sending = false;
            if (response.errorNumber !== 1) {
                PSModal.open({
                    text: `<p>Error enviando su mensaje (${response.errorCode}) : ${response.errorMessage} </p>`,
                    actionText: 'Aceptar',
                    actionCallback: () => {return false;}
                });
            } else {
                PSModal.open({
                    text: `<p>Su mensaje ha sido enviado exitosamente</p>`,
                    actionText: 'Aceptar',
                    actionCallback: () => {return false;}
                });
                form.reset();
                this.sector = '';
            }
        });
    }

    private validateForm() {
        const requiredFields = {
            nombre: 'nombre',
            apellido: 'apellido',
            email: 'e-mail de contacto',
            codigoPostal: 'código postal',
            consulta: 'el motivo de su consulta',
        };

        const missingFields : Array<any> = [];
        for (const requiredField in requiredFields) {
            if (! this[requiredField]) {
                missingFields.push(requiredFields[requiredField]);
            }
        }

        if (!this.telefonoContacto && !this.telefonoMovilContacto) {
            missingFields.push('su teléfono fijo o móvil');
        }

        if (missingFields.length) {
            let errorMessage = "Debe ingresar " + missingFields.join(', ');
            let pos = errorMessage.lastIndexOf(',');
            errorMessage = errorMessage.substring(0,pos) + errorMessage.substring(pos).replace(',', ' y');
            errorMessage += '.';
            this.formError(errorMessage);
            return false;
        }

        if (!this.validator.validateEmail(this.email)) {
            this.formError("El e-mail ingresado no es válido.");
            return false;
        }

        if (this.telefonoMovilContacto && !this.validator.validateTelMovil(this.telefonoMovilContacto)) {
            this.formError("El teléfono móvil ingresado no es válido");
            return false;
        }

        if (this.telefonoContacto && !this.validator.validateTelFijo(this.telefonoContacto)) {
            this.formError("El teléfono ingresado no es válido");
            return false;
        }

        if (!this.validator.validateCodigoPostal(this.codigoPostal)) {
            this.formError("El código postal ingresado no es válido");
            return false;
        }

        return true;
    }

    private formError(errorMessage : string) {
        PSModal.open({
            text: `<p>${errorMessage}</p>`,
            actionText: 'Aceptar',
            actionCallback: () => {return false;}
        });
    }
}