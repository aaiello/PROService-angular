import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'sec-contacta-telefono',
    templateUrl: 'contacta-telefono-sec.component.html',
})
export class ContactaTelefonoSecComponent {

    codigoPostal : string;

    constructor(private router : Router) {

    }

    findClosePunto() {
        this.router.navigate(['/puntos-proservice', this.codigoPostal]);
        return false;
    }
}