import { Component, ElementRef, AfterViewInit } from '@angular/core';
declare var $:any;

@Component({
    selector: 'sec-gc-como_lo_hacemos',
    templateUrl: 'gc-como_lo_hacemos-sec.component.html',
})
export class GCComoLoHacemosSecComponent implements AfterViewInit {

    constructor(private ref:ElementRef) { }

    ngAfterViewInit() {

        var $tabpanes = $(this.ref.nativeElement).find('.tabpanes');

        $tabpanes.each(function() {
            var $content = $(this).find('.content');
            $(this).find('.tabs li').on('click', function() {
                $(this).siblings('.selected').removeClass('selected');
                $(this).addClass('selected');
                $content.find('.pane.active').removeClass('active');
                $content.find('#' + $(this).attr('data-tab')).addClass('active');
            });
        });
    }
}