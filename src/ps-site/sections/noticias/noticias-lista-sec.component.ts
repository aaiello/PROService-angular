import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NoticiaService } from "../../services/ps-noticia.service";
import { Noticia } from '../../services/ps-noticia';
import { NoticiaList } from "../../services/ps-noticia-list";

@Component({
    selector: 'sec-noticias-lista',
    templateUrl: 'noticias-lista-sec.component.html',
    providers: [ NoticiaService ]
})
export class NoticiasListaSecComponent {

    noticias : Array<Noticia>;
    searchTerms : string;

    constructor(private router : Router,
                private route : ActivatedRoute,
                private noticiaService: NoticiaService) {
    }

    ngOnInit() {

        this.route.data.subscribe((data:any) => {

            this.noticias = data.noticiaList.noticias;
            if(data.noticiaList.searchTerm) {
                this.searchTerms = data.noticiaList.searchTerm;
            }
        });
    }
}