import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { MetaService } from '@nglibs/meta';
import { NoticiaService } from "../../services/ps-noticia.service";
import { Noticia } from '../../services/ps-noticia';

@Component({
    selector: 'sec-noticias-detalle',
    templateUrl: 'noticias-detalle-sec.component.html',
    providers: [ MetaService, NoticiaService ]
})
export class NoticiasDetalleSecComponent {

    noticia : Noticia;
    lastUpdateAction : string;
    lastUpdateDate : Date;

    constructor(
        private router : Router,
        private route : ActivatedRoute,
        private meta : MetaService,
        private noticiaService : NoticiaService) {
    }

    ngOnInit() {
        this.route.data.subscribe((data:any) => {
            this.noticia = data.noticia;
            this.lastUpdateDate =
                this.noticia.fechaActualizacion > this.noticia.fechaPublicacion ?
                    this.noticia.fechaActualizacion : this.noticia.fechaPublicacion;
            this.lastUpdateAction =
                this.noticia.fechaActualizacion > this.noticia.fechaPublicacion ?
                    'Actualizado' : 'Publicado';
            this.meta.setTitle(this.noticia.titulo);
        });
    }
}