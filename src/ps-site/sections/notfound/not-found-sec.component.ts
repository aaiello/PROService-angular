import { Component } from '@angular/core';

@Component({
    selector: 'sec-not-found',
    templateUrl: 'not-found-sec.component.html',
})
export class NotFoundSecComponent { }