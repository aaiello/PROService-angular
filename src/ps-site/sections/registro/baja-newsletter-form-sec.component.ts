import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from "@angular/router";
import { PSBajaNewsletterService } from '../../services/ps-baja-newsletter.service';
import { NgForm } from "@angular/forms";
declare let PSModal : any;

@Component({
    selector: 'sec-baja-newsletter-form',
    templateUrl: 'baja-newsletter-form-sec.component.html',
    providers: [ PSBajaNewsletterService ]
})
export class BajaNewsletterFormSecComponent implements OnInit {

    sending : boolean = false;
    done : boolean = false;

    email : string = '';
    confirma : boolean = false;

    constructor(private activatedRoute: ActivatedRoute,
                private bajaService : PSBajaNewsletterService) { }

    ngOnInit() {
        this.activatedRoute.queryParams.subscribe((params: Params) => {
            this.email = params['email'];
        });
    }

    submitForm(form : NgForm) {
        if (this.sending) {
            return;
        }

        if (!this.validateForm()) {
            return;
        }

        this.sending = true;
        this.bajaService.send({
            email: this.email,
        }).subscribe((response : any) => {
            this.sending = false;
            if (response.errorNumber !== 1) {
                PSModal.open({
                    text: `<p>Error enviando su registro (${response.errorCode}) : ${response.errorMessage} </p>`,
                    actionText: 'Aceptar',
                    actionCallback: () => {return false;}
                });
            } else {
                this.done = true;
                form.reset();
            }
        });
    }

    private validateForm() {

        if (!this.confirma) {
            this.formError("Debe confirmar que desea darse de baja haciendo click en el casillero.")
            return false;
        }

        if (!this.email) {
            this.formError("La solicitud es inválida (no se recibió la dirección de email como argumento)");
            return false;
        }

        return true;
    }

    private formError(errorMessage : string) {
        PSModal.open({
            text: `<p>${errorMessage}</p>`,
            actionText: 'Aceptar',
            actionCallback: () => { return false; }
        });
    }
}