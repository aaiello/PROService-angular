import { NgModule, APP_INITIALIZER } from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { FormsModule }   from '@angular/forms';
import { AppConfig }       from './app.config';
import { PSSiteComponent } from './ps-site.component';
import { PSRoutingModule } from "./ps-routing.module";

import { InicioPageComponent } from "./pages/inicio-page.component";
import { PageNotFoundComponent } from "./pages/not-found.component";
import { AcercaPageComponent } from "./pages/acerca-page.component";
import { AcercaCompromisoSecComponent } from "./sections/acerca/acerca-compromiso-sec.component";
import { AcercaProAProSecComponent } from "./sections/acerca/acerca-pro_a_pro-sec.component";
import { AcercaTuPuntoSecComponent } from "./sections/acerca/acerca-tu_punto-sec.component";
import { ContactaPageComponent } from "./pages/contacta-page.component";
import { GrandesCuentasPageComponent } from "./pages/grandes_cuentas-page.component";
import { ProductosPageComponent } from "./pages/productos-page.component";
import { UnetePageComponent } from "./pages/unete-page.component";
import { VentajasPageComponent } from "./pages/ventajas-page.component";
import { VinculacionPageComponent } from "./pages/vinculacion-page.component";
import { RegistroPageComponent } from "./pages/registro-page.component";
import { RegistroFormSecComponent } from "./sections/registro/registro-form-sec.component";
import { BajaNewsletterPageComponent } from "./pages/baja-newsletter-page.component";
import { BajaNewsletterFormSecComponent } from "./sections/registro/baja-newsletter-form-sec.component";
import { SuscripcionNewsletterPageComponent } from "./pages/suscripcion-newsletter-page.component";
import { SuscripcionNewsletterFormSecComponent } from "./sections/registro/suscripcion-newsletter-form-sec.component";
import { SoyMecanicoPageComponent } from "./pages/soymecanico-page.component";
import { SoyMecanicoFormSecComponent } from "./sections/soymecanico/soymecanico-form-sec.component";
import { NoticiasListaPageComponent } from "./pages/noticias-lista-page.component";
import { NoticiasDetallePageComponent } from "./pages/noticias-detalle-page.component";
import { NoticiasListaSecComponent } from "./sections/noticias/noticias-lista-sec.component";
import { NoticiasDetalleSecComponent } from "./sections/noticias/noticias-detalle-sec.component";
import { FooterBlockComponent } from "./blocks/footer.component";
import { HeaderBlockComponent } from "./blocks/header.component";
import { HeaderSoloLogoBlockComponent } from "./blocks/header-solologo.component";
import { FooterDisclamerBlockComponent } from "./blocks/footer-disclamer.component";
import { FooterDisclamerMeetingpointBlockComponent } from "./blocks/footer-disclamer-meetingpoint.component";
import { InicioAccordionSecComponent } from "./sections/inicio/inicio-accordion-sec.component";
import { InicioMasInfoSecComponent } from "./sections/inicio/inicio-mas_info-sec.component";
import { InicioProAProSecComponent } from "./sections/inicio/inicio-pro_a_pro-sec.component";
import { InicioSeesawSecComponent } from "./sections/inicio/inicio-seesaw-sec.component";
import { InicioSliderSecComponent } from "./sections/inicio/inicio-slider-sec.component";
import { InicioTrabajarSecComponent } from "./sections/inicio/inicio-trabajar-sec.component";
import { InicioUltimasNoticiasSecComponent } from "./sections/inicio/inicio-ultimas_noticias-sec.component";
import { InicioUneteSecComponent } from "./sections/inicio/inicio-unete-sec.component";
import { InicioYoutubeSecComponent } from "./sections/inicio/inicio-youtube-sec.component";
import { InicioProductosSecComponent } from "./sections/inicio/inicio-productos-sec.component";
import { InicioVentajasSecComponent } from "./sections/inicio/inicio-ventajas-sec.component";
import { ContactaEmailSecComponent } from "./sections/contacta/contacta-email-sec.component";
import { ContactaTelefonoSecComponent } from "./sections/contacta/contacta-telefono-sec.component";
import { GCComoLoHacemosSecComponent } from "./sections/grandes_cuentas/gc-como_lo_hacemos-sec.component";
import { GCQueBuscamosSecComponent } from "./sections/grandes_cuentas/gc-que_buscamos-sec.component";
import { GCVentajasSecComponent } from "./sections/grandes_cuentas/gc-ventajas-sec.component";
import { ProductosListaSecComponent } from "./sections/productos/productos-lista-sec.component";
import { ProductosServicioSecComponent } from "./sections/productos/productos-servicio-sec.component";
import { UnetePasosSecComponent } from "./sections/unete/unete-pasos-sec.component";
import { UnetePremioSecComponent } from "./sections/unete/unete-premio-sec.component";
import { VentajasListaSecComponent } from "./sections/ventajas/ventajas-lista-sec.component";
import { VentajasPorQueSecComponent } from "./sections/ventajas/ventajas-por_que-sec.component";
import { VinculacionSecComponent } from "./sections/vinculacion/vinculacion-sec.component";
import { NotFoundSecComponent } from "./sections/notfound/not-found-sec.component";
import { OpenProGironaSecComponent } from"./sections/registro/open-pro-girona-form-sec.component";
import { OpenProBarcelonaoestSecComponent } from"./sections/registro/open-pro-barcelonaoest-form-sec.component";
import { OpenProTarragonaSecComponent } from"./sections/registro/open-pro-tarragona-form-sec.component";

import { PSValidation } from "./services/ps-validation.service";
import { NoticiaService } from "./services/ps-noticia.service";
import { OpenProGironaPageComponent } from "./pages/open-pro-girona-page.component";
import { OpenProBarcelonaoestPageComponent } from "./pages/open-pro-barcelonaoest-page.component";
import { OpenProTarragonaPageComponent } from "./pages/open-pro-tarragona-page.component";

import { FormularioMeetingpointPageComponent } from "./formularios/formulario-meetingpoint-page.component";
import { FormularioMeetingpointSecComponent } from"./formularios/formulario-meetingpoint-sec.component";

@NgModule({
    imports: [
        BrowserModule,
        HttpModule,
        FormsModule,
        PSRoutingModule,
    ],
    declarations: [
        PSSiteComponent,
        FooterBlockComponent,
        HeaderBlockComponent,
        HeaderSoloLogoBlockComponent,
        FooterDisclamerBlockComponent,
        FooterDisclamerMeetingpointBlockComponent,
        InicioPageComponent,
        InicioAccordionSecComponent,
        InicioMasInfoSecComponent,
        InicioProAProSecComponent,
        InicioSeesawSecComponent,
        InicioSliderSecComponent,
        InicioTrabajarSecComponent,
        InicioUltimasNoticiasSecComponent,
        InicioUneteSecComponent,
        InicioYoutubeSecComponent,
        InicioProductosSecComponent,
        InicioVentajasSecComponent,
        AcercaPageComponent,
        AcercaCompromisoSecComponent,
        AcercaProAProSecComponent,
        AcercaTuPuntoSecComponent,
        ContactaPageComponent,
        ContactaEmailSecComponent,
        ContactaTelefonoSecComponent,
        GrandesCuentasPageComponent,
        GCComoLoHacemosSecComponent,
        GCQueBuscamosSecComponent,
        GCVentajasSecComponent,
        ProductosPageComponent,
        ProductosListaSecComponent,
        ProductosServicioSecComponent,
        UnetePageComponent,
        UnetePasosSecComponent,
        UnetePremioSecComponent,
        VentajasPageComponent,
        VentajasListaSecComponent,
        VentajasPorQueSecComponent,
        VinculacionPageComponent,
        VinculacionSecComponent,
        RegistroPageComponent,
        RegistroFormSecComponent,
        BajaNewsletterPageComponent,
        BajaNewsletterFormSecComponent,
        SuscripcionNewsletterPageComponent,
        SuscripcionNewsletterFormSecComponent,
        SoyMecanicoPageComponent,
        SoyMecanicoFormSecComponent,
        NoticiasListaPageComponent,
        NoticiasListaSecComponent,
        NoticiasDetallePageComponent,
        NoticiasDetalleSecComponent,
        PageNotFoundComponent,
        NotFoundSecComponent,
        OpenProGironaSecComponent,
        OpenProGironaPageComponent,
        OpenProBarcelonaoestSecComponent,
        OpenProBarcelonaoestPageComponent,
        OpenProTarragonaSecComponent,
        OpenProTarragonaPageComponent,
        FormularioMeetingpointPageComponent,
        FormularioMeetingpointSecComponent,
    ],
    providers: [
        AppConfig,
        NoticiaService,
        { provide: APP_INITIALIZER, useFactory: (config: AppConfig) => () => config.load(), deps: [AppConfig], multi: true }
    ],
    bootstrap: [ PSSiteComponent ]
})
export class PSSiteModule { }