import { NgModule } from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { HovertoggleModule } from './hovertoggle/index';

@NgModule({
    imports: [
        BrowserModule,
        HovertoggleModule
    ],
    declarations: [
        AppComponent
    ],
    bootstrap: [ AppComponent ]
})
export class AppModule { }