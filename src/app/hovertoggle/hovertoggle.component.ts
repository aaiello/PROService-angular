import { Component } from '@angular/core';
import './hovertoggle.component.css';
@Component({
    selector: 'at-hovertoggle',
    templateUrl: './hovertoggle.component.html',
    styleUrls: ['./hovertoggle.component.css']
})
export class HovertoggleComponent {

    isOn : boolean = false;

    constructor() {

    }

    onMouseOver() {
        this.isOn = true;
    }

    onMouseOut() {
        this.isOn = false;
    }
}