import { Component } from '@angular/core';

@Component({
    selector: 'at-hovertoggle-on',
    template: '<ng-content></ng-content>',
})
export class HovertoggleOnComponent { }