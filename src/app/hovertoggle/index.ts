import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {HovertoggleComponent} from "./hovertoggle.component";
import {HovertoggleOnComponent} from "./hovertoggle-on.component";
import {HovertoggleOffComponent} from "./hovertoggle-off.component";

export * from "./hovertoggle.component";
export * from "./hovertoggle-on.component";
export * from "./hovertoggle-off.component";

@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [
        HovertoggleComponent,
        HovertoggleOnComponent,
        HovertoggleOffComponent
    ],
    exports: [
        HovertoggleComponent,
        HovertoggleOnComponent,
        HovertoggleOffComponent
    ]
})
export class HovertoggleModule {

}