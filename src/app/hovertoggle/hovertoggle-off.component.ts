import { Component } from '@angular/core';

@Component({
    selector: 'at-hovertoggle-off',
    template: '<ng-content></ng-content>',
})
export class HovertoggleOffComponent { }